import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from 'react-redux';
import store from './store';
import * as Constants from './utils/Constants'
import * as Colors from './res/Colors'
import { Offline, Online } from "react-detect-offline";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import LoginSignup from '../src/screens/LoginSignup'
import HomePage from '../src/screens/HomePage'
import Search from '../src/screens/Search'
import Offers from '../src/screens/Offers'
import Help from '../src/screens/Help'
import * as Utility from '../src/utils/Utility'
import HomeRoute from '../src/utils/HomeRoute'
import BaseComponent from "./common/BaseComponent";
import * as StringKeys from './res/StringKeys'

const hist = createBrowserHistory();
const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#fff',
            main: Colors.colorPrimary,
            dark: Colors.colorPrimaryDark
        },
        secondary: {
            main: Colors.colorPrimaryDark,
        },

    },
    typography: {
        useNextVariants: true
    }
});

class App extends BaseComponent {
    render() {
        return (
            <MuiThemeProvider theme={theme}>
                <Fragment>
                    <Online>
                        <Provider store={store} >
                            <Router history={hist}>
                                <Switch>
                                    <Route path={Constants.SCREEN_LOGIN} component={LoginSignup} /> 
                                    <HomeRoute path={Constants.SCREEN_HOME} component={HomePage} />
                                    <HomeRoute path={Constants.SCREEN_SEARCH} component={Search} />                                    
                                    <HomeRoute path={Constants.SCREEN_OFFERS} component={Offers} />
                                    <HomeRoute path={Constants.SCREEN_HELP} component={Help} />                                                                                                          
                                    <Redirect from="/" to={Constants.SCREEN_LOGIN} /> 
                                </Switch>
                            </Router>
                        </Provider>
                    </Online>
                    <Offline>
                        <div>{this.strings(StringKeys.You_Are_Offline)} </div>
                    </Offline>
                </Fragment>
            </MuiThemeProvider>
        );
    }
}

export default App;