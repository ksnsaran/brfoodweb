import * as ApiUrls from './ApiUrls';
import * as types from './types';
import * as Constants from '../utils/Constants';
import * as CustomStorage from '../utils/CustomStorage';
var METHOD_TYPE_POST = 'post';
var METHOD_TYPE_GET = 'get';

export const getFormDataFromObject = (data) => {
  const formData = new FormData();
  for (var key in data) {
    if (typeof data[key] === 'object') {
      var dataValue = data[key];
      if (key === Constants.KEY_IMAGES_ARRAY) {
        for (var itemIndex in data[key]) {
          var keyName = Constants.KEY_IMAGES_ARRAY + itemIndex + Constants.KEY_ARRAY_CLOSE;
          formData.append(keyName, data[key][itemIndex]);
        }
      }
      else if (key == Constants.KEY_DOCUMENTS_ARRAY) {
        for (var itemIndex in data[key]) {
          var keyName = Constants.KEY_DOCUMENTS_ARRAY + itemIndex + Constants.KEY_ARRAY_CLOSE;
          formData.append(keyName, data[key][itemIndex]);
        }
      }
      else if (key == Constants.KEY_IMAGE) {
        if (dataValue != undefined && dataValue != null) {
          formData.append(key, dataValue);
        }
      }
      else {
        if (dataValue !== null && dataValue.uri !== undefined && dataValue.uri !== null) {
        }
        else {
          if (dataValue != null) {
            dataValue = ((JSON.stringify(dataValue)));
            dataValue = dataValue.replace(/\\/g, '');
          }
        }
        if (dataValue != undefined && dataValue != null) {
          formData.append(key, dataValue);
        }
      }
    }
    else {
      if (data[key] != undefined && data[key] != null) {
        formData.append(key, data[key]);
      }
    }
  }
  return formData;
}


export const isValidResponse = (responseTmp, dispatch, apiType, context) => {
  var isValid = true;
  var msg = '';
  var response = JSON.parse(responseTmp);
  console.log('apiType ' + apiType + ' response : ', response);
  if (response) {
    if (response.hasOwnProperty(Constants.KEY_ERROR) && response.hasOwnProperty(Constants.KEY_MESSAGE) && response[Constants.KEY_ERROR] === true) {
      if (response[Constants.KEY_MESSAGE] !== null && response[Constants.KEY_MESSAGE] !== '')
        msg = response[Constants.KEY_MESSAGE];
    }
  }

  if (msg !== '') {
    isValid = false;
    if (context !== undefined && context.handleResponse !== undefined) {
      context.handleResponse({ [Constants.KEY_SHOW_PROGRESS]: false, type: apiType })
    }
    else {
      dispatch({ type: 'reload', payload: { [Constants.KEY_SHOW_PROGRESS]: false, type: apiType } });
    }
    setTimeout(() => {
      alert(msg);
    }, 200)
  }
  else {
  

    if (context !== undefined && context.handleResponse !== undefined) {
    

      context.handleResponse({ [Constants.KEY_RESPONSE]: response, type: apiType })
    }
    else {
   

     dispatch({ payload: { [Constants.KEY_RESPONSE]: response, type: apiType } });
    }
  }
  return isValid;
}
export const callApiFinal = (data, dispatch, apiUrl, typeValue, methodType, showPBar, context) => {
  if (context !== undefined && context.handleResponse !== undefined) {
    context.handleResponse({ [Constants.KEY_SHOW_PROGRESS]: showPBar, type: typeValue })
  }
  else {
    dispatch({ type: 'reload', payload: { [Constants.KEY_SHOW_PROGRESS]: showPBar, type: typeValue } });
  }
  var reqObj = {
    method: methodType
  };
  const userData = CustomStorage.getSessionDataAsObject(Constants.KEY_USER_DATA);

  console.log('global[Constants.KEY_USER_DATA] Saddam 1', userData)
  if (userData !== undefined && userData !== null) {
    reqObj[Constants.KEY_HEADERS] = {
      'Authorization': 'Bearer ' + userData[Constants.KEY_TOKEN],
    }
  }
  if (methodType === METHOD_TYPE_POST) {
    if (data !== undefined && data !== null) {
      console.log('requestData: ' + JSON.stringify(data) + ' API Post: ' + apiUrl);
      const formData = getFormDataFromObject(data);
      reqObj[Constants.KEY_BODY] = formData;
    }

    fetch(apiUrl, reqObj).then(response => response.text())
      .then(res => isValidResponse(res, dispatch, typeValue, context)).catch(error => {
        if (context !== undefined && context.handleResponse !== undefined) {
          context.handleResponse({ [Constants.KEY_SHOW_PROGRESS]: false, type: typeValue })
        }
        else {
          dispatch({ type: 'reload', payload: { [Constants.KEY_SHOW_PROGRESS]: false, type: typeValue } });
        }
        setTimeout(() => {
          alert( error);
        }, 200)
      })
  }
  else if (methodType === METHOD_TYPE_GET) {
    var finalUrl = apiUrl;
    console.log('requestData: ' + ' API GEt: ' + finalUrl);
    fetch(finalUrl, reqObj).then(response => response.text())
      .then(res => isValidResponse(res, dispatch, typeValue, context)).catch(error => {
        if (context !== undefined && context.handleResponse !== undefined) {
          context.handleResponse({ [Constants.KEY_SHOW_PROGRESS]: false, type: typeValue })
        }
        else {
          dispatch({ type: 'reload', payload: { [Constants.KEY_SHOW_PROGRESS]: false, type: typeValue } });
        }
        setTimeout(() => {
          alert( error);
        }, 200)
      })
  }
}
export const callApiAfterNetChecking = async (data, dispatch, apiUrl, typeValue, methodType, isConnected, showPBar, context) => {
  if (isConnected) {
    callApiFinal(data, dispatch, apiUrl, typeValue, methodType, showPBar, context);
  } else {
    // enableDisabledClickedBtn(context, false);
    alert('Network is not available');
  }
}

export const callApi = async (data, dispatch, apiUrl, typeValue, methodType, showPBar, context) => {
  try {
    callApiAfterNetChecking(data, dispatch, apiUrl, typeValue, methodType, true, showPBar, context);
  } catch (e) {
    console.log('exception', e);
  }
}

export const objectToFormData = (obj, form, namespace) => {
  var fd = form || new FormData();
  var formKey;
  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {

      if (namespace) {
        formKey = namespace + '[' + property + ']';
      } else {
        formKey = property;
      }
      // if the property is an object, but not a File,
      // use recursivity.
      if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
        objectToFormData(obj[property], fd, property);
      } else {
        // if it's a string or a File object
        fd.append(formKey, obj[property]);
      }

    }
  }
  return fd;
}

export const reqSendOtp = (data, context) => async (dispatch) => {

  callApi(data, dispatch, ApiUrls.API_SEND_OTP,
    types.API_SEND_OTP, METHOD_TYPE_POST, true, context);
}

export const reqVerifyOtp = (data, context) => async (dispatch) => {

  callApi(data, dispatch, ApiUrls.API_VERIFY_OTP,
    types.API_VERIFY_OTP, METHOD_TYPE_POST, true, context);
}

export const reqHomepage = (data, context) => async (dispatch) => {

  callApi(data, dispatch, ApiUrls.API_HOMEPAGE,
    types.API_HOMEPAGE, METHOD_TYPE_POST, true, context);
}


// export const enableDisabledClickedBtn = (context, disabled) => {
//   if (context.state.disabledClickedBtn !== undefined) {
//     context.setState({ disabledClickedBtn: disabled })
//   }
// }