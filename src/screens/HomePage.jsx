import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Footer, NavBar } from '../common';
import Slider from "react-slick";
import * as ResConst from '../res/ResourcesConstants'
import $ from 'jquery'
import * as types from '../actions/types';
import { reqHomepage } from '../actions'
import * as Constants from '../utils/Constants'
import CustomPBar from '../common/CustomPBar'
import BaseComponent from '../common/BaseComponent';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as Stringkeys from '../res/StringKeys'
import '../../css/easy-responsive-tabs.css'


var settings = {
  dots: false,
  infinite: false,
  speed: 150,
  slidesToShow: 4,
  slidesToScroll: 2,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 2,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
};

var images = [1, 2, 3]

const data = undefined;
class HomePage extends BaseComponent {


  constructor(props) {
    super(props);
    this.state = {
      [Constants.KEY_SHOW_PROGRESS]: true,
      [Constants.KEY_RESPOBJ]: undefined
    }
   // this.getLocation();
  }

  componentDidMount() {

    setTimeout(() => {
      this.props.reqHomepage({
        [Constants.KEY_OFFSET]: 0,
        [Constants.KEY_LAT]: 26.858726299999997,
        [Constants.KEY_LNG]: 75.7720215,
        // [Constants.KEY_LAT]: this.state[Constants.KEY_LAT],
        // [Constants.KEY_LNG]: this.state[Constants.KEY_LNG],
        [Constants.KEY_ROLE]: Constants.KEY_USER
      }, this)
    }, 10);

  }


  toggle(divId) {

    $("#" + divId).show();
  }

  closeModal(divId) {
    $("#" + divId).hide()
  }

  // getMyLocation = () => {
  //   console.log("current lat lng permission navigator", navigator);


  getLocation = () => {




    if (navigator.geolocation) {
      // timeout at 60000 milliseconds (60 seconds)
      var options = { timeout: 60000 };
      navigator.geolocation.getCurrentPosition((position) => {

        this.setState({
          [Constants.KEY_LAT]: position.coords.latitude,
          [Constants.KEY_LNG]: position.coords.longitude
        })

      },
        (err) => {

          if (err.code == 1) {
            alert("Error: Access is denied!");
          }
          else if (err.code == 2) {
            alert("Error: Position is unavailable!");
          }

        }, options);
    } else {
      alert("Sorry, browser does not support geolocation!");
    }
  }


  handleResponse = (nextProps) => {

    var respObj = null;
    if (nextProps.hasOwnProperty(Constants.KEY_SHOW_PROGRESS)) {
      respObj = { [Constants.KEY_SHOW_PROGRESS]: nextProps[Constants.KEY_SHOW_PROGRESS] };
      this.setState(respObj)
    }
    else if (nextProps.hasOwnProperty(Constants.KEY_RESPONSE)) {

      if (nextProps[Constants.KEY_TYPE] === types.API_HOMEPAGE) {

        if (nextProps[Constants.KEY_RESPONSE].hasOwnProperty(Constants.KEY_DATA)) {
          console.log('handleResponse called in nextProps Add Category Scree : ', nextProps.response)
          data = nextProps.response.data;
        }

        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, [Constants.KEY_VISIBLE_OTP]: true, [Constants.KEY_DATA]: data };
        this.setState(respObj);
      }
    }
  }

  render() {
    const { data } = this.state;
    return (
      <Fragment>
        <Fragment>

          <div className="slider-right location-left hidden" id="location">
            <button className="close-btn" onClick={() => this.closeModal('location')}><i><img src="images/close-icon.png" /></i></button>
            <div className="clearfix"></div>
            <div className="location-part">
              <div className="location-add-input">
                <input type="text" id="" name="" placeholder="Search for area, street name.." className="input-box" />
              </div>
              <div className="gps-box">
                <i><img src="images/locat-icon.png" /></i>
                <h5><a href="#">{this.strings(Stringkeys.Get_current_location)}</a></h5>
                <small>{this.strings(Stringkeys.Using_GPS)}</small>
              </div>

              <div className="address-add">
                <h6>{this.strings(Stringkeys.SAVED_ADDRESSES)}</h6>
                <h5><a href="#">{this.strings(Stringkeys.Work)}</a></h5>
                <small>90 Usha Vihar, 10 B Scheme, Vishveshvariya Nagar, Arjun Nagar, Jaipur, Rajasthan 302018, India</small>
                <div className="clearfix bdrtop"></div>
                <h5><a href="#">{this.strings(Stringkeys.Other)}</a></h5>
                <small>63 shiv raj nikalen vaishali, Parijaat Lane, Shivraj Niketan Colony, Vaishali Nagar, Jaipur, Rajasthan 302021, India</small>
              </div>
            </div>
          </div>

          <div className="slider-right location-left hidden" id="location">
            <button className="close-btn" onClick={() => this.closeModal('location')}><i><img src="images/close-icon.png" /></i></button>
            <div className="clearfix"></div>
            <div className="location-part">
              <div className="location-add-input">
                <input type="text" id="" name="" placeholder="Search for area, street name.." className="input-box" />
              </div>
              <div className="gps-box">
                <i><img src="images/locat-icon.png" /></i>
                <h5><a href="#">{this.strings(Stringkeys.Get_current_location)}</a></h5>
                <small>{this.strings(Stringkeys.Using_GPS)}</small>
              </div>

              <div className="address-add">
                <h6>{this.strings(Stringkeys.SAVED_ADDRESSES)}</h6>
                <h5><a href="#">{this.strings(Stringkeys.Work)}</a></h5>
                <small>90 Usha Vihar, 10 B Scheme, Vishveshvariya Nagar, Arjun Nagar, Jaipur, Rajasthan 302018, India</small>
                <div className="clearfix bdrtop"></div>
                <h5><a href="#">{this.strings(Stringkeys.Other)}</a></h5>
                <small>63 shiv raj nikalen vaishali, Parijaat Lane, Shivraj Niketan Colony, Vaishali Nagar, Jaipur, Rajasthan 302021, India</small>
              </div>
            </div>
          </div>

          <div className="slider-right hidden" id="filter">
            <button className="close-btn" onClick={() => this.closeModal('filter')}><i><img src={ResConst.close_icon} /></i></button>
            <div className="login-part">
              <h1>{this.strings(Stringkeys.Filters)}</h1>
              <div className="filter-checkbox">
                <div className="row">
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox1" type="checkbox" checked="" />
                      <label htmlFor="checkbox1">
                        American
                                           </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox2" type="checkbox" checked="" />
                      <label htmlFor="checkbox2">
                        Awadhi
                                           </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox3" type="checkbox" checked="" />
                      <label htmlFor="checkbox3">
                        Bakery
                                             </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox4" type="checkbox" checked="" />
                      <label htmlFor="checkbox4">
                        Beverages
                                            </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox5" type="checkbox" checked="" />
                      <label htmlFor="checkbox5">
                        Biryani
                                              </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox6" type="checkbox" checked="" />
                      <label htmlFor="checkbox6">
                        Cafe
                                             </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox7" type="checkbox" checked="" />
                      <label htmlFor="checkbox7">
                        Chaat
                                            </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox8" type="checkbox" checked="" />
                      <label htmlFor="checkbox8">
                        Chinese
                                             </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox23" type="checkbox" checked="" />
                      <label htmlFor="checkbox23">
                        Combo
                                            </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox9" type="checkbox" checked="" />
                      <label htmlFor="checkbox9">
                        Continental
                                             </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox10" type="checkbox" checked="" />
                      <label htmlFor="checkbox10">
                        Desserts
                                             </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox11" type="checkbox" checked="" />
                      <label htmlFor="checkbox11">
                        Fast Food
                                              </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox12" type="checkbox" checked="" />
                      <label htmlFor="checkbox12">
                        French
                                             </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox13" type="checkbox" checked="" />
                      <label htmlFor="checkbox13">
                        Healthy Food
                                           </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox14" type="checkbox" checked="" />
                      <label htmlFor="checkbox14">
                        Ice Cream
                                            </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox15" type="checkbox" checked="" />
                      <label htmlFor="checkbox15">
                        Indian
                                            </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox16" type="checkbox" checked="" />
                      <label htmlFor="checkbox16">
                        Italian
                                            </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox17" type="checkbox" checked="" />
                      <label htmlFor="checkbox17">
                        Juices
                                           </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox18" type="checkbox" checked="" />
                      <label htmlFor="checkbox18">
                        Kebabs
                                          </label>
                    </div>
                  </div>

                  <div className="col-12 mt-3">
                    <h5>Show Restaurants With</h5>
                  </div>

                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox19" type="checkbox" checked="" />
                      <label htmlFor="checkbox19">
                        Offers
                                           </label>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="checkbox checkbox-danger">
                      <input id="checkbox20" type="checkbox" checked="" />
                      <label htmlFor="checkbox20">
                        Pure Veg
                                           </label>
                    </div>
                  </div>

                </div>
              </div>

              <div className="row filter-box">
                <div className="col-sm-5">
                  <button className="Submit-btn gray-btn">{this.strings(Stringkeys.CLEAR)}</button>
                </div>
                <div className="col-sm-7">
                  <button className="Submit-btn">{this.strings(Stringkeys.SHOW_RESTAURANTS)}</button>
                </div>
              </div>
            </div>
          </div>
        </Fragment>


        <Fragment>
          {/* navigation section */}

          {/* <NavBar context={this} /> */}

          {/* navigation section */}
          {/* banner section */}
          <div className="banner-part " style={{ marginTop: '60px' }}>
            {this.showLinearPBar(this.state[Constants.KEY_SHOW_PROGRESS])}

            {data != undefined && data.coupon_code != undefined && data.coupon_code.length > 0 ?
              <div className="container" >
                <Slider {...settings}>
                  {data.coupon_code.map((item, index) =>
                    (
                      <div key={index}>
                        <div className="offer-col">
                          <img src={item.image} />
                          <span className="offer-tag">{this.strings(Stringkeys.Offer)}</span>
                          <h3>{item.title}<i className="fa fa-rupee"></i> {item.coupon_minimum_discount_amount}</h3>
                        </div>
                      </div>
                    )
                  )}
                </Slider>
              </div> : null}
          </div>
          {/* banner section */}

          {/* section one */}

          <section className="brfood-part">
            <div className="container">
              <div className="row">
                <div className="col-sm-6">
                  <div className="food-pop">
                    <div className="row">
                      <div className="col-sm-3 pr-0">
                        <div className="pop-img">
                          {images.map((id) =>
                            (
                              <img key={id} src={ResConst.offer_img} className="round-img img1" />
                            ))}
                          {/* <img src="images/offer-img.jpg" className="round-img img1" />
                                                    <img src="images/offer-img.jpg" className="round-img img2" />
                                                    <img src="images/offer-img.jpg" className="round-img img3" /> */}
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="pop-head">
                          <h4>{this.strings(Stringkeys.Introducing_BR)}</h4>
                          <p>{this.strings(Stringkeys.Single_Serve_Meals)}</p>
                        </div>
                      </div>
                      <div className="col-sm-3">
                        <button>{this.strings(Stringkeys.Order_Now)}</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6">
                  <div className="food-pop">
                    <div className="row">
                      <div className="col-sm-2 pr-0">
                        <div className="pop-img">
                          <img src={ResConst.logo_icon} />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="pop-head">
                          <h4>{this.strings(Stringkeys.Introducing_BR_Food_SUPER)}</h4>
                          <p>{this.strings(Stringkeys.Single_Serve_Meals)}</p>
                        </div>
                      </div>
                      <div className="col-sm-4">
                        <button>{this.strings(Stringkeys.Get_Super_Now)}</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          {/* section one */}

          {/* section Two */}
          <section className="restaurant-part">
            <div className="container">
              <div className="inner-heading">
                <div className="row">
                  <div className="col-sm-6">
                    <h1>{this.state.data != undefined ? this.state.data.total : null} {this.strings(Stringkeys.Restaurants)}</h1>
                  </div>
                  <div className="col-sm-6">
                    <ul className="filter-menu">
                      <li><a href="#">{this.strings(Stringkeys.Relevance_Cost)}</a></li>
                      <li><a href="#">{this.strings(Stringkeys.for_Two)}</a></li>
                      <li><a href="#">{this.strings(Stringkeys.Delivery_Time)}</a></li>
                      <li><a href="#">{this.strings(Stringkeys.Rating)}</a></li>
                      <li className="filter-col"><a href="#" onClick={() => this.toggle('filter')}>{this.strings(Stringkeys.Filter)} <i className="fa fa-filter"></i></a></li>
                    </ul>
                  </div> 
                </div>
              </div>
              <div className="row">

                {this.state.data != undefined && this.state.data.restroList != undefined ?
                  this.state.data.restroList.map((item, index) => (
                    <div key={index} className="col-sm-3">
                      <div className="restaurant-col">
                        <img src={item.image != undefined ? item.image : null} />
                        <span className="offer-tag1">{this.strings(Stringkeys.Promoted)}</span>
                        <div className="restra-inner">
                          <h3>{item.name}</h3>
                          {item.categories.map((data, index) => {
                            if (index > 1) {
                              return null
                            } else {
                              return <p>{data}</p>
                            }
                          })}

                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> {item.rating_from_user.toFixed(1)}</span>
                            </li>
                            <li>
                              {(item.duration_sec / 60).toFixed(0)} {this.strings(Stringkeys.MINS)}
                            </li>
                            <li>
                              <i className="fa fa-rupee"></i> {item.cost_for_two} {this.strings(Stringkeys.For_Two)}
                            </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                  ))
                  : null}


              </div>                                                                                                                                              </div>
          </section>


          <Footer />

        </Fragment>
      </Fragment>
    );
  }
}

function mapStateToProps({ response }) {
  return response;
}


export default connect(mapStateToProps, { reqHomepage })(HomePage);

