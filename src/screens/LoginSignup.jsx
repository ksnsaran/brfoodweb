import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as Constants from '../utils/Constants';
import * as CustomStorage from '../utils/CustomStorage';
import * as Utility from '../utils/Utility';
import BaseComponent from '../common/BaseComponent';
import CustomPBar from '../common/CustomPBar';
import * as StringKeys from '../res/StringKeys';
import { connect } from 'react-redux';
import * as types from '../actions/types';
import * as Colors from '../res/Colors'
import { reqSendOtp, reqVerifyOtp } from '../actions'
import $ from 'jquery'
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import LocationSearchInput from '../common/LocationSearchInput'
import * as ResConst from '../res/ResourcesConstants'

const styles = {
  locationView: {
    width: 500,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center'
  }
};



class Login extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      [Constants.KEY_SHOW_PROGRESS]: false,
      [Constants.KEY_PHONE]: '',
      [Constants.KEY_VISIBLE_OTP]: false,
      [Constants.KEY_OTP]: '',
      [Constants.KEY_DISABLE_BUTTON]: true,
      [Constants.KEY_ADDRESS]: ''
    }
  }


  submit = event => {
    event.preventDefault();
  }

  toggleModal(divId) {
    if (divId == 'login') {
      this.setState({
        loginColor: Colors.red_btn_clr,
        signupBttn: Colors.white,
        loginTextColor: Colors.white,
        signupTextColor: Colors.black
      })
      $("#" + divId).show();
      $("#" + 'signup').hide();
    }
    if (divId == 'signup') {
      this.setState({
        loginColor: Colors.white,
        signupBttn: Colors.red_btn_clr,
        loginTextColor: Colors.black,
        signupTextColor: Colors.white
      })
      $("#" + divId).show();
      $("#" + 'login').hide();
    }
  }

  closeModal(divId) {
    $("#" + divId).hide()
    this.setState({
      loginColor: Colors.white,
      signupBttn: Colors.white,
      loginTextColor: Colors.black,
      signupTextColor: Colors.black
    })
  }

  handleChange = input => e => {
    console.log('input', input.length)
    if (e.target.value.length > 9) {
      this.setState({ disabledButton: false })
    }
    console.log('value', e.target.value)
    this.setState({
      [input]: e.target.value
    })
  }


  sendOtp = () => {
    this.setState({ disabledButton: true })
    if (this.state.visibleOtp) {
      this.props.reqVerifyOtp({
        [Constants.KEY_COUNTRY_CODE]: Constants.KEY_COUNTRY_CODE_VALUE,
        [Constants.KEY_PHONE]: this.state[Constants.KEY_PHONE],
        [Constants.KEY_DEVICE_ID]: '44f8a30f30ad1b59',
        [Constants.KEY_DEVICE_TYPE]: 'web',
        [Constants.KEY_DEVICE_TOKEN]: 'dfdsf',
        [Constants.KEY_OTP]: this.state[Constants.KEY_OTP],
        [Constants.KEY_ROLE]: Constants.KEY_USER

      }, this)

    } else {
      this.props.reqSendOtp({
        [Constants.KEY_COUNTRY_CODE]: Constants.KEY_COUNTRY_CODE_VALUE,
        [Constants.KEY_PHONE]: this.state[Constants.KEY_PHONE],
        [Constants.KEY_ROLE]: Constants.KEY_USER
      }, this)
    }
  }

  render() {
    const { classes } = this.props;
    const { visibleOtp, } = this.state;
    return (
      <Fragment>

        {this.showLinearPBar(this.state[Constants.KEY_SHOW_PROGRESS])}

        <div className="slider-right hidden" id="login">
          <button className="close-btn" onClick={() => this.closeModal('login')} ><i><img src={ResConst.close_icon} /></i></button>
          <div className="login-part">
            <h1>{this.strings(StringKeys.Login)}</h1>

            <p>{this.strings(StringKeys.Or)} <a href="#" onClick={() => this.toggleModal('signup')}> {this.strings(StringKeys.Create_An_Account)}</a></p>

            <input
              id={Constants.KEY_PHONE}
              type="text"
              disabled={visibleOtp}
              value={this.state[Constants.KEY_PHONE]}
              maxLength={Constants.MAX_LENGTH_PHONE}
              onChange={this.handleChange(Constants.KEY_PHONE)}
              // placeholder={this.strings(StringKeys.Phone_Number)}
               placeholder="Phone Number"
              className="input-box" />

            {
              visibleOtp &&
              (<input
                id={Constants.KEY_OTP}
                type="text"
                value={this.state[Constants.KEY_OTP]}
                maxLength={Constants.MAX_LENGTH_OTP}
                onChange={this.handleChange(Constants.KEY_OTP)}
                placeholder="One time password"
                className="input-box" />)
            }

            <button style={{ backgroundColor: this.state.disabledButton ? Colors.desabled_btn_clr : Colors.red_btn_clr }}
              disabled={this.state.disabledButton}
              className="Submit-btn" onClick={this.sendOtp}>{visibleOtp ? this.strings(StringKeys.Verify_Otp) : this.strings(StringKeys.Login)}</button>

          </div>

        </div>

        <div className="slider-right hidden" id="signup">
          <button className="close-btn" onClick={() => this.closeModal('signup')} ><i><img src={ResConst.close_icon} /></i></button>

          <div className="login-part">
            <h1>{this.strings(StringKeys.Sign_Up)}</h1>
            <p>{this.strings(StringKeys.Or)}<a href="#" onClick={() => this.toggleModal('login')}> {this.strings(StringKeys.Login_To_Your_Account)}</a></p>

            <input type="text" id="" name="" placeholder="Phone Number" className="input-box" />
            <input type="text" id="" name="" placeholder="Name " className="input-box" />
            <input type="text" id="" name="" placeholder="Email" className="input-box" />
            <input type="text" id="" name="" placeholder="Password" className="input-box" />
            <input type="text" id="" name="" placeholder="Referral Code" className="input-box" />

            <button className="Submit-btn">{this.strings(StringKeys.Continue)}</button>

            <p className="mt-2">{this.strings(StringKeys.By_Creating_An_Account)} <a href="#">{this.strings(StringKeys.Terms_Conditions)}</a></p>

          </div>

        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="col-xl-6 col-lg-12">
              <div className="login-location">
                <div className="login-header">
                  <div className="row">
                    <div className="col-4">
                      <div className="logo"><img src={ResConst.logo} /></div>
                    </div>

                    <div className="col-8">
                      <ul className="login-nav">
                        <li style={{ backgroundColor: this.state.loginColor }}>
                          <a
                            style={{ color: this.state.loginTextColor }}
                            href="#"
                            onClick={() => { this.toggleModal('login') }}>{this.strings(StringKeys.Login)}
                          </a>
                        </li>
                        <li style={{ backgroundColor: this.state.signupBttn }}>
                          <a
                            style={{ color: this.state.signupTextColor }}
                            href="#"
                            onClick={() => this.toggleModal('signup')}>{this.strings(StringKeys.Signup)}</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="heading-text">
                  <h1>{this.strings(StringKeys.Hungry)}</h1>
                  <p>{this.strings(StringKeys.Order_Food_From_Favourite)}</p>
                </div>

                {/*
                    <div className="search-location-part">
                      <input type="text" name="" id="" placeholder={this.strings(StringKeys.Enter_Your_Delivery_Location)} className="location-input" />
                      <input type="text" name="" id="" placeholder="Locat Me" className="location-input2" />
                      <button className="btn find-food-btn">{this.strings(StringKeys.FIND_FOOD)}</button>
                    </div>  
        */}

                <LocationSearchInput />

                <h3 className="city-head">{this.strings(StringKeys.Popular_Cities)}</h3>
                <ul className="city-name">
                  <li><a href="#">{this.strings(StringKeys.Ahmedabad)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Bangalore)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Chennai)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Delhi)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Gurgaon)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Hyderabad)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Kolkata)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Mumbai)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Pune)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.More)}</a></li>
                </ul>
              </div>
            </div>
            <div className="col-xl-6 col-lg-12">
              <div className="login-header-img"> <img src={ResConst.login_img} /> </div>
            </div>
          </div>
        </div>
        <section className="howToProcess">
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-4 col-md-4 col-sm-12">
                <div className="process-box"> <i><img src={ResConst.No_min_order} /></i>
                  <h3>{this.strings(StringKeys.No_Minimum_Order)}</h3>
                  <p>{this.strings(StringKeys.Order_In_For)}</p>
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-12">
                <div className="process-box"> <i><img src={ResConst.Live_order} /></i>
                  <h3>{this.strings(StringKeys.Live_Order_Tracking)}</h3>
                  <p>{this.strings(StringKeys.Know_Where_Your)}</p>
                </div>
              </div>
              <div className="col-lg-4 col-md-4 col-sm-12">
                <div className="process-box"> <i><img src={ResConst.Live_order} /></i>
                  <h3>{this.strings(StringKeys.Lightning_Fast_Delivery)}</h3>
                  <p>{this.strings(StringKeys.Experience_Br_Food)}</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="appPart">
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <div className="app-content">
                  <h1>{this.strings(StringKeys.Restaurants_In_Your_Pocket)}</h1>
                  <p>{this.strings(StringKeys.Order_From_Your)}</p>
                  <div className="mobile-app"> <a href="#"><img src={ResConst.play_icon} /></a> <a href="#"><img src={ResConst.iOS_icon} /></a> </div>
                </div>
              </div>
              <div className="col-md-6 col-sm-12">
                <div className="app-img"> <img src={ResConst.app_img} /> </div>
              </div>
            </div>
          </div>
        </section>

        <footer>
          <div className="container">
            <div className="row footer-col1">
              <div className="col-sm-3">
                <h3>{this.strings(StringKeys.COMPANY)}</h3>
                <ul>
                  <li><a href="#">{this.strings(StringKeys.About_Us)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Team)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Careers)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Blog)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Bug_Bounty)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Pop)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food)}</a></li>
                </ul>
              </div>
              <div className="col-sm-3">
                <h3>{this.strings(StringKeys.CONTACT)}</h3>
                <ul>
                  <li><a href="#">{this.strings(StringKeys.Help_Support)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Partner_with_us)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Ride_with_us)}</a></li>
                </ul>
              </div>
              <div className="col-sm-3">
                <h3>{this.strings(StringKeys.LEGAL)}</h3>
                <ul>
                  <li><a href="#">{this.strings(StringKeys.Terms_Conditions)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Refund_Cancellation)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Privacy_Policy)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Cookie_Policy)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Offer_Terms)}</a></li>
                </ul>
              </div>
              <div className="col-sm-3">
                <div className="footer-app-icon">
                  <div className="mobile-app"> <a href="#"><img src={ResConst.play_icon} /></a> <a href="#"><img src={ResConst.iOS_icon} /></a> </div>
                </div>
              </div>
            </div>
            <div className="row footer-col1">
              <div className="col-sm-12 mb-2">
                <h3>{this.strings(StringKeys.WE_DELIVER_TO)}</h3>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li><a href="#">{this.strings(StringKeys.About_Us)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Team)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Careers)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Blog)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Bug_Bounty)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Pop)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food)}</a></li>
                </ul>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li><a href="#">{this.strings(StringKeys.About_Us)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Team)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Careers)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Blog)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Bug_Bounty)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Pop)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food)}</a></li>
                </ul>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li><a href="#">{this.strings(StringKeys.About_Us)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Team)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Careers)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Blog)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Bug_Bounty)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Pop)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food)}</a></li>
                </ul>
              </div>
              <div className="col-sm-3">
                <ul>
                  <li><a href="#">{this.strings(StringKeys.About_Us)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Team)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Careers)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Blog)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.Bug_Bounty)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food_Pop)}</a></li>
                  <li><a href="#">{this.strings(StringKeys.BR_Food)}</a></li>
                </ul>
              </div>
            </div>
            <div className="copyright">
              <div className="row">
                <div className="col-sm-12 text-center">
                  <p>&copy; {this.strings(StringKeys.Copyright_2019)}</p>
                  <ul>
                    <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </footer>

      </Fragment>
    );
  }

  handleResponse = (nextProps) => {

    var respObj = null;
    if (nextProps.hasOwnProperty(Constants.KEY_SHOW_PROGRESS)) {
      respObj = { [Constants.KEY_SHOW_PROGRESS]: nextProps[Constants.KEY_SHOW_PROGRESS] };
      this.setState(respObj)
    }
    else if (nextProps.hasOwnProperty(Constants.KEY_RESPONSE)) {

      if (nextProps[Constants.KEY_TYPE] === types.API_SEND_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)

        this.setState({ disabledButton: false, [Constants.KEY_OTP]: nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA][Constants.KEY_OTP] });

        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: true, };
        this.setState(respObj);

      } else if (nextProps[Constants.KEY_TYPE] === types.API_VERIFY_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)
        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: false };

        this.setState(respObj);

        if (nextProps[Constants.KEY_RESPONSE].hasOwnProperty(Constants.KEY_DATA)) {

          CustomStorage.setSessionDataAsObject(Constants.KEY_USER_DATA, nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA]);

          window.location = Constants.SCREEN_HOME;
        }


      }
    }
  }

}


function mapStateToProps({ response }) {
  return response;
}



export default connect(
  mapStateToProps, { reqSendOtp, reqVerifyOtp }
)(withStyles(styles)(Login));
// export default connect(mapStateToProps, { reqSendOtp, reqVerifyOtp })(Login);
