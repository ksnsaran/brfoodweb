import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as Constants from '../utils/Constants';
import * as CustomStorage from '../utils/CustomStorage';
import * as Utility from '../utils/Utility';
import BaseComponent from '../common/BaseComponent';
import CustomPBar from '../common/CustomPBar';
import * as StringKeys from '../res/StringKeys';
import { connect } from 'react-redux';
import * as types from '../actions/types';
import * as Colors from '../res/Colors'
import { reqSendOtp, reqVerifyOtp } from '../actions'
import $ from 'jquery'
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Footer, NavBar } from '../common';
import * as ResConst from '../res/ResourcesConstants'

class Search extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      [Constants.KEY_SHOW_PROGRESS]: false,
      [Constants.KEY_PHONE]: '',
      visibleOtp: false,
      [Constants.KEY_OTP]: '',
      disabledButton: true
    }
  }

  render() {
    const { classNames } = this.props;

    return (
      <Fragment>
        {/* <NavBar context={this} /> */}

        <section className="search_sec">
          <div className="container">
            <div className="row">
              <div className="col-sm-11">
                <div className="input-group md-form form-sm form-1 pl-0">
                  <div className="input-group-prepend"> <span className="input-group-text purple lighten-3" id="basic-text1"><i className="fa fa-search" aria-hidden="true"></i></span> </div>
                  <input className="form-control my-0 py-1" type="text" placeholder="Search" aria-label="Search" />
                  <div className="input-group-prepend clear"> <span className="input-group-text purple lighten-3" id="basic-text1"><a href="">Clear</a></span> </div>
                </div>
              </div>
              <div className="col-sm-1">
                <div href="" className="esc"> <i><img src={ResConst.close_bt} /></i> <span>ESC</span> </div>
              </div>
            </div>
          </div>
        </section>
        <section className="restaurant-part offer-part srch_tab">
          <div className="container">
            <div id="horizontalTab">
              <ul className="resp-tabs-list">
                <li>Restaurants</li>
                <li>Dishes</li>
              </ul>
              <div className="slects_box"> <span>
                <select>
                  <option>Relevance </option>
                  <option>Delivery Time </option>
                  <option>Rating </option>
                  <option>Cost for Two</option>
                </select>
              </span> </div>
              <div className="resp-tabs-container restaurant_lst">
                <div className="tab1">
                  <div className="_27-i_">Related to "<span className="_1qtJs">kachori</span>"</div>
                  <div className="row">
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating red-color"><i className="fa fa-star"></i> 3.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating red-color"><i className="fa fa-star"></i> 3.2</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3 ">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4 col-xl-3">
                      <div className="restaurant-col"> <img src={ResConst.restra_img} /> <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <p>Indian, Chines</p>
                          <ul>
                            <li> <span className="rating"><i className="fa fa-star"></i> 4.1</span> </li>
                            <li> 28 MINS </li>
                            <li> <i className="fa fa-rupee"></i> 200 For Two </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount}/></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="tab2">
                  <ul className="breadcrumb">
                    <li className="breadcrumb-item"><a href="#">Suggestions</a></li>
                    <li className="breadcrumb-item active"><a href="#">Kalakand</a></li>
                  </ul>
                  <div className="row dish_lsit">
                    <div className="col-sm-12">
                      <div className="dishes_panel">
                        <div className="m_bg">
                          <h4>Bombay Mishthan Bhandar-sweets <span className="se_menu"><a href="">See Menu</a></span></h4>
                          <p>Swets, Snacks, Bakery</p>
                          <div className="spc"> <span><i className="fa fa-star" aria-hidden="true"></i> 4.3</span> - <span>32 Mins</span> - <span><i className="fa fa-inr" aria-hidden="true"></i> 200 For Two</span> </div>
                        </div>
                        <ul className="lst">
                          <li>
                            <div className="row">
                              <div className=" col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className=" col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="row dish_lsit">
                    <div className="col-sm-12">
                      <div className="dishes_panel">
                        <div className="m_bg">
                          <h4>Bombay Mishthan Bhandar-sweets <span className="se_menu"><a href="">See Menu</a></span></h4>
                          <p>Swets, Snacks, Bakery</p>
                          <div className="spc"> <span><i className="fa fa-star" aria-hidden="true"></i> 4.3</span> - <span>32 Mins</span> - <span><i className="fa fa-inr" aria-hidden="true"></i> 200 For Two</span> </div>
                        </div>
                        <ul className="lst">
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="row dish_lsit">
                    <div className="col-sm-12">
                      <div className="dishes_panel">
                        <div className="m_bg">
                          <h4>Bombay Mishthan Bhandar-sweets <span className="offer-tag2">Exclusive</span> <span className="se_menu"><a href="">See Menu</a></span></h4>
                          <p>Swets, Snacks, Bakery</p>
                          <div className="spc"> <span><i className="fa fa-star" aria-hidden="true"></i> 4.3</span> - <span>32 Mins</span> - <span><i className="fa fa-inr" aria-hidden="true"></i> 200 For Two</span> </div>
                        </div>
                        <ul className="lst">
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                          <li>
                            <div className="row">
                              <div className="col-sm-10">
                                <div className="veg_icon"><img src={ResConst.veg_icon} /></div>
                                <div className="cat_hd">
                                  <h5>Butterscotch Kalakand</h5>
                                  <span><i className="fa fa-inr" aria-hidden="true"></i> 105</span> </div>
                              </div>
                              <div className="col-sm-2">
                                <div className="rg_sec">
                                  <button type="button" className="btn">Add +</button>
                                  <span>Customisable</span> </div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </Fragment>
    );
  }

  handleResponse = (nextProps) => {

    var respObj = null;
    if (nextProps.hasOwnProperty(Constants.KEY_SHOW_PROGRESS)) {
      respObj = { [Constants.KEY_SHOW_PROGRESS]: nextProps[Constants.KEY_SHOW_PROGRESS] };
      this.setState(respObj)
    }
    else if (nextProps.hasOwnProperty(Constants.KEY_RESPONSE)) {

      if (nextProps[Constants.KEY_TYPE] === types.API_SEND_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)

        this.setState({ disabledButton: false, [Constants.KEY_OTP]: nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA][Constants.KEY_OTP] });

        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: true, };
        this.setState(respObj);

      } else if (nextProps[Constants.KEY_TYPE] === types.API_VERIFY_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)
        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: false };

        this.setState(respObj);

        if (nextProps[Constants.KEY_RESPONSE].hasOwnProperty(Constants.KEY_DATA)) {

          CustomStorage.setSessionDataAsObject(Constants.KEY_USER_DATA, nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA]);

          window.location = Constants.SCREEN_HOME;
        }


      }
    }
  }

}


function mapStateToProps({ response }) {
  return response;
}


export default connect(mapStateToProps, { reqSendOtp, reqVerifyOtp })(Search);
