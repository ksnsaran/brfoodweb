import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as Constants from '../utils/Constants';
import * as CustomStorage from '../utils/CustomStorage';
import * as Utility from '../utils/Utility';
import BaseComponent from '../common/BaseComponent';
import CustomPBar from '../common/CustomPBar';
import * as StringKeys from '../res/StringKeys';
import { connect } from 'react-redux';
import * as types from '../actions/types';
import * as Colors from '../res/Colors'
import { reqSendOtp, reqVerifyOtp } from '../actions'
import $ from 'jquery'
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Footer, NavBar } from '../common';
import * as ResConst from '../res/ResourcesConstants'
import Tabs from 'react-responsive-tabs';
import 'react-responsive-tabs/styles.css';


const presidents = [{ name: 'George Washington', biography: '.dsfadfa..' }, { name: 'Theodore Roosevelt', biography: '.dadfdafdas..' },
  { name: 'George Washington', biography: '.dsfadfa..' }, { name: 'Theodore Roosevelt', biography: '.dadfdafdas..' },
  { name: 'George Washington', biography: '.dsfadfa..' }, { name: 'Theodore Roosevelt', biography: '.dadfdafdas..' }
];

class Help extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      [Constants.KEY_SHOW_PROGRESS]: false,
      [Constants.KEY_PHONE]: '',
      visibleOtp: false,
      [Constants.KEY_OTP]: '',
      disabledButton: true
    }
  }

 getTabs() {
  return presidents.map((president, index) => ({
    title: president.name,
    getContent: () => president.biography,
    /* Optional parameters */
    key: index,
    tabClassName: 'tab',
    panelClassName: 'panel',
  }));
}

  render() {
    const { className } = this.props;

    return (
      <Fragment>
        <body className="inner-part">

          {/* <NavBar context={this} /> */}


          <section className="profile-part">
            <div className="container">
              <div className="profile-name-part">
                <div className="row">
                  <div className=" col-sm-8 text-left">
                    <h2>Help & Support</h2>
                    <p>Let's take a step ahead and help you better.</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="profile-content">
                <div className="row">
                  <div className="col-sm-12">
                    <div id="verticalTab">
                      {/* <ul className="resp-tabs-list">
                        <li>Help with orders</li>
                        <li>General issues</li>
                        <li>Legal & Terms & Conditions</li>
                        <li>FAQs</li>
                        <li>SUPER FAQs</li>
                        <li>Conversation archive</li>
                      </ul> */}

                      <Tabs items={this.getTabs()} />
                                       
                      <div className="resp-tabs-container">
                        <div className="tab1">
                          <h3 className="mb-3">Past Orders</h3>
                          <div className="order-box">
                            <div className="row">
                              <div className="col-sm-2 dish-lf_img">
                                <div className="dish-img"><img src={ResConst.restra_img} /></div>
                              </div>
                              <div className="col-sm-6">
                                <div className="order-dish-details">
                                  <h5>Sunrise Bakers & Confectioners</h5>
                                  <p>Bais Godam Lal-Kothi</p>
                                  <p>ORDER #35636546058 | Thu, Mar 14, 4:32 PM</p>
                                  <a href="#">VIEW DETAILS</a> </div>
                              </div>
                              <div className="col-sm-4"> <a href="#" className="Delivered-text">Delivered on Thu, Mar 14, 5:02 PM <i className="fa fa-check-circle"></i></a> </div>
                              <div className="col-sm-12">
                                <div className="bdrtop2"></div>
                              </div>
                              <div className="col-6 text">
                                <p>Aloo Patties x 9</p>
                              </div>
                              <div className="col-6 text-right">
                                <p>Total Paid: <i className="fa fa-rupee"></i> 152</p>
                              </div>
                              <div className="col-12">
                                <button className="reoder-btn">Get help</button>
                              </div>
                            </div>
                          </div>
                          <div className="order-box">
                            <div className="row">
                              <div className="col-sm-2 dish-lf_img">
                                <div className="dish-img"><img src={ResConst.restra_img} /></div>
                              </div>
                              <div className="col-sm-6">
                                <div className="order-dish-details">
                                  <h5>Sunrise Bakers & Confectioners</h5>
                                  <p>Bais Godam Lal-Kothi</p>
                                  <p>ORDER #35636546058 | Thu, Mar 14, 4:32 PM</p>
                                  <a href="#">VIEW DETAILS</a> </div>
                              </div>
                              <div className="col-sm-4"> <a href="#" className="Delivered-text">Delivered on Thu, Mar 14, 5:02 PM <i className="fa fa-check-circle"></i></a> </div>
                              <div className="col-sm-12">
                                <div className="bdrtop2"></div>
                              </div>
                              <div className="col-6 text">
                                <p>Aloo Patties x 9</p>
                              </div>
                              <div className="col-6 text-right">
                                <p>Total Paid: <i className="fa fa-rupee"></i> 152</p>
                              </div>
                              <div className="col-12">
                                <button className="reoder-btn">Get help</button>
                              </div>
                            </div>
                          </div>
                          <div className="order-box">
                            <div className="row">
                              <div className="col-sm-2 dish-lf_img">
                                <div className="dish-img"><img src={ResConst.restra_img} /></div>
                              </div>
                              <div className="col-sm-6">
                                <div className="order-dish-details">
                                  <h5>Sunrise Bakers & Confectioners</h5>
                                  <p>Bais Godam Lal-Kothi</p>
                                  <p>ORDER #35636546058 | Thu, Mar 14, 4:32 PM</p>
                                  <a href="#">VIEW DETAILS</a> </div>
                              </div>
                              <div className="col-sm-4"> <a href="#" className="Delivered-text">Delivered on Thu, Mar 14, 5:02 PM <i className="fa fa-check-circle"></i></a> </div>
                              <div className="col-sm-12">
                                <div className="bdrtop2"></div>
                              </div>
                              <div className="col-6 text">
                                <p>Aloo Patties x 9</p>
                              </div>
                              <div className="col-6 text-right">
                                <p>Total Paid: <i className="fa fa-rupee"></i> 152</p>
                              </div>
                              <div className="col-12">
                                <button className="reoder-btn">Get help</button>
                              </div>
                            </div>
                          </div>
                          <div className="order-box">
                            <div className="row">
                              <div className="col-sm-2 dish-lf_img">
                                <div className="dish-img"><img src={ResConst.restra_img} /></div>
                              </div>
                              <div className="col-sm-6">
                                <div className="order-dish-details">
                                  <h5>Sunrise Bakers & Confectioners</h5>
                                  <p>Bais Godam Lal-Kothi</p>
                                  <p>ORDER #35636546058 | Thu, Mar 14, 4:32 PM</p>
                                  <a href="#">VIEW DETAILS</a> </div>
                              </div>
                              <div className="col-sm-4"> <a href="#" className="Delivered-text">Delivered on Thu, Mar 14, 5:02 PM <i className="fa fa-check-circle"></i></a> </div>
                              <div className="col-sm-12">
                                <div className="bdrtop2"></div>
                              </div>
                              <div className="col-6 text">
                                <p>Aloo Patties x 9</p>
                              </div>
                              <div className="col-6 text-right">
                                <p>Total Paid: <i className="fa fa-rupee"></i> 152</p>
                              </div>
                              <div className="col-12">
                                <button className="reoder-btn">Get help</button>
                              </div>
                            </div>
                          </div>
                          <div className="order-box">
                            <div className="row">
                              <div className="col-sm-2 dish-lf_img">
                                <div className="dish-img"><img src={ResConst.restra_img} /></div>
                              </div>
                              <div className="col-sm-6">
                                <div className="order-dish-details">
                                  <h5>Sunrise Bakers & Confectioners</h5>
                                  <p>Bais Godam Lal-Kothi</p>
                                  <p>ORDER #35636546058 | Thu, Mar 14, 4:32 PM</p>
                                  <a href="#">VIEW DETAILS</a> </div>
                              </div>
                              <div className="col-sm-4"> <a href="#" className="Delivered-text">Delivered on Thu, Mar 14, 5:02 PM <i className="fa fa-check-circle"></i></a> </div>
                              <div className="col-sm-12">
                                <div className="bdrtop2"></div>
                              </div>
                              <div className="col-6 text">
                                <p>Aloo Patties x 9</p>
                              </div>
                              <div className="col-6 text-right">
                                <p>Total Paid: <i className="fa fa-rupee"></i> 152</p>
                              </div>
                              <div className="col-12">
                                <button className="reoder-btn">Get help</button>
                              </div>
                            </div>
                          </div>
                          <div className="order-box">
                            <div className="row">
                              <div className="col-sm-2 dish-lf_img">
                                <div className="dish-img"><img src={ResConst.restra_img} /></div>
                              </div>
                              <div className="col-sm-6">
                                <div className="order-dish-details">
                                  <h5>Sunrise Bakers & Confectioners</h5>
                                  <p>Bais Godam Lal-Kothi</p>
                                  <p>ORDER #35636546058 | Thu, Mar 14, 4:32 PM</p>
                                  <a href="#">VIEW DETAILS</a> </div>
                              </div>
                              <div className="col-sm-4"> <a href="#" className="Delivered-text">Delivered on Thu, Mar 14, 5:02 PM <i className="fa fa-check-circle"></i></a> </div>
                              <div className="col-sm-12">
                                <div className="bdrtop2"></div>
                              </div>
                              <div className="col-6 text">
                                <p>Aloo Patties x 9</p>
                              </div>
                              <div className="col-6 text-right">
                                <p>Total Paid: <i className="fa fa-rupee"></i> 152</p>
                              </div>
                              <div className="col-12">
                                <button className="reoder-btn">Get help</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="tab2">
                          <h3 className="mb-3">Favourites</h3>
                          <div className="accordion accordian_sec" id="myAccordion">
                            <div className="card">
                              <div className="card-header" id="item1Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link " type="button" data-toggle="collapse" data-target="#expandable1" aria-expanded="false" aria-controls="expandable1"> I want to unsubscribe from BR Food communications </button>
                                </h5>
                              </div>
                              <div id="expandable1" className="collapse show" aria-labelledby="item1Header" data-parent="#myAccordion">
                                <div className="card-body">
                                  <p>Please drop us an email mentioning the channels you would like to be unsubscribed from. We will take an action and confirm within 24-48 hours</p>
                                  <div className="bt_sec"> <a className="rd_bt" href="">CALL US (0605412803)</a>
                                    <div className="wt">Wait time under 1 min(s)</div>
                                  </div>
                                  <div className="bt_sec"> <a className="rd_bt" href="">CHAT WITH US</a>
                                    <div className="wt">Wait time under 3 min(s)</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="item2Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#expandable2" aria-expanded="true" aria-controls="expandable2"> I have a coupon related query </button>
                                </h5>
                              </div>
                              <div id="expandable2" className="collapse " aria-labelledby="item2Header" data-parent="#myAccordion">
                                <div className="card-body"> This is the card body. Your content is hidden initially. It is shown by clicking on the card header. </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="item3Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#expandable3" aria-expanded="false" aria-controls="expandable3"> EI want to unsubscribe from BR Food communications </button>
                                </h5>
                              </div>
                              <div id="expandable3" className="collapse" aria-labelledby="item3Header" data-parent="#myAccordion">
                                <div className="card-body"> This is the card body. Your content is hidden initially. It is shown by clicking on the card header. </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="tab3">
                          <h3 className="mb-3">Legal & Terms & Conditions</h3>
                          <div className="accordion accordian_sec" id="myAccordion1">
                            <div className="card">
                              <div className="card-header" id="item01Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link " type="button" data-toggle="collapse" data-target="#expandable01" aria-expanded="false" aria-controls="expandable01"> Terms of Use </button>
                                </h5>
                              </div>
                              <div id="expandable01" className="collapse show" aria-labelledby="item01Header" data-parent="#myAccordion1">
                                <div className="card-body">
                                  <p>These terms of use (the "Terms of Use") govern your use of our website www.brfood.com (the "Website") and our "brfood" application for mobile and handheld devices (the "App"). The Website and the App are jointly referred to as the "Services"). Please read these Terms of Use carefully before you download, install or use the Services. If you do not agree to these Terms of Use, you may not install, download or use the Services. By installing, downloading or using the Services, you signify your acceptance to the Terms of Use and Privacy Policy (being hereby incorporated by reference herein) which takes effect on the date on which you download, install or use the Services, and create a legally binding arrangement to abide by the same.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="item02Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#expandable02" aria-expanded="true" aria-controls="expandable02"> Privacy Policy </button>
                                </h5>
                              </div>
                              <div id="expandable02" className="collapse " aria-labelledby="item02Header" data-parent="#myAccordion1">
                                <div className="card-body">
                                  <p>These terms of use (the "Terms of Use") govern your use of our website www.brfood.com (the "Website") and our "brfood" application for mobile and handheld devices (the "App"). The Website and the App are jointly referred to as the "Services"). Please read these Terms of Use carefully before you download, install or use the Services. If you do not agree to these Terms of Use, you may not install, download or use the Services. By installing, downloading or using the Services, you signify your acceptance to the Terms of Use and Privacy Policy (being hereby incorporated by reference herein) which takes effect on the date on which you download, install or use the Services, and create a legally binding arrangement to abide by the same.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="item03Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#expandable03" aria-expanded="false" aria-controls="expandable03"> Cancellations and Refunds </button>
                                </h5>
                              </div>
                              <div id="expandable03" className="collapse" aria-labelledby="item03Header" data-parent="#myAccordion1">
                                <div className="card-body">
                                  <p>These terms of use (the "Terms of Use") govern your use of our website www.brfood.com (the "Website") and our "brfood" application for mobile and handheld devices (the "App"). The Website and the App are jointly referred to as the "Services"). Please read these Terms of Use carefully before you download, install or use the Services. If you do not agree to these Terms of Use, you may not install, download or use the Services. By installing, downloading or using the Services, you signify your acceptance to the Terms of Use and Privacy Policy (being hereby incorporated by reference herein) which takes effect on the date on which you download, install or use the Services, and create a legally binding arrangement to abide by the same.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="item04Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#expandable04" aria-expanded="false" aria-controls="expandable04"> Terms of Use for BR Food ON-TIME / Assured </button>
                                </h5>
                              </div>
                              <div id="expandable04" className="collapse" aria-labelledby="item04Header" data-parent="#myAccordion1">
                                <div className="card-body">
                                  <p>These terms of use (the "Terms of Use") govern your use of our website www.brfood.com (the "Website") and our "brfood" application for mobile and handheld devices (the "App"). The Website and the App are jointly referred to as the "Services"). Please read these Terms of Use carefully before you download, install or use the Services. If you do not agree to these Terms of Use, you may not install, download or use the Services. By installing, downloading or using the Services, you signify your acceptance to the Terms of Use and Privacy Policy (being hereby incorporated by reference herein) which takes effect on the date on which you download, install or use the Services, and create a legally binding arrangement to abide by the same.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="tab4">
                          <h3 className="mb-3">FAQs</h3>
                          <div className="accordion accordian_sec" id="myAccordion2">
                            <div className="card">
                              <div className="card-header" id="faq01Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link " type="button" data-toggle="collapse" data-target="#faq01" aria-expanded="false" aria-controls="faq01"> I entered the wrong CVV, why did my transaction still go through? </button>
                                </h5>
                              </div>
                              <div id="faq01" className="collapse show" aria-labelledby="faq01Header" data-parent="#myAccordion2">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="faq02Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq02" aria-expanded="true" aria-controls="faq02"> I want to partner my restaurant with BR Food </button>
                                </h5>
                              </div>
                              <div id="faq02" className="collapse " aria-labelledby="faq02Header" data-parent="#myAccordion2">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="faq03Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq03" aria-expanded="false" aria-controls="faq03"> I want to explore career opportunities with BR Food </button>
                                </h5>
                              </div>
                              <div id="faq03" className="collapse" aria-labelledby="faq03Header" data-parent="#myAccordion2">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="faq04Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq04" aria-expanded="false" aria-controls="faq04"> I want to provide feedback </button>
                                </h5>
                              </div>
                              <div id="faq04" className="collapse" aria-labelledby="faq04Header" data-parent="#myAccordion2">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="faq05Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq05" aria-expanded="false" aria-controls="faq05"> I want to cancel my order </button>
                                </h5>
                              </div>
                              <div id="faq05" className="collapse" aria-labelledby="faq05Header" data-parent="#myAccordion2">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="faq06Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#faq06" aria-expanded="false" aria-controls="faq06"> Will BR Food be accountable for quality/quantity? </button>
                                </h5>
                              </div>
                              <div id="faq06" className="collapse" aria-labelledby="faq06Header" data-parent="#myAccordion2">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="tab4">
                          <h3 className="mb-3">SUPER FAQs</h3>
                          <div className="accordion accordian_sec" id="myAccordion3">
                            <div className="card">
                              <div className="card-header" id="sfaq01Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link " type="button" data-toggle="collapse" data-target="#sfaq01" aria-expanded="false" aria-controls="sfaq01"> What is BR Food Super? </button>
                                </h5>
                              </div>
                              <div id="sfaq01" className="collapse show" aria-labelledby="sfaq01Header" data-parent="#myAccordion3">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="sfaq02Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#sfaq02" aria-expanded="true" aria-controls="sfaq02"> How can I subscribe to Food Super? </button>
                                </h5>
                              </div>
                              <div id="sfaq02" className="collapse " aria-labelledby="sfaq02Header" data-parent="#myAccordion3">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="sfaq03Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#sfaq03" aria-expanded="false" aria-controls="sfaq03"> What are the benefits of subscribing to the program? </button>
                                </h5>
                              </div>
                              <div id="sfaq03" className="collapse" aria-labelledby="sfaq03Header" data-parent="#myAccordion3">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="sfaq04Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#sfaq04" aria-expanded="false" aria-controls="sfaq04"> How do I renew my subscription? When will I be able to renew? </button>
                                </h5>
                              </div>
                              <div id="sfaq04" className="collapse" aria-labelledby="sfaq04Header" data-parent="#myAccordion3">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="sfaq05Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#sfaq05" aria-expanded="false" aria-controls="sfaq05"> How do I cancel my subscription? </button>
                                </h5>
                              </div>
                              <div id="sfaq05" className="collapse" aria-labelledby="sfaq05Header" data-parent="#myAccordion3">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header" id="sfaq06Header">
                                <h5 className="mb-0">
                                  <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#sfaq06" aria-expanded="false" aria-controls="sfaq06"> What is the duration of this subscription service? </button>
                                </h5>
                              </div>
                              <div id="sfaq06" className="collapse" aria-labelledby="sfaq06Header" data-parent="#myAccordion3">
                                <div className="card-body">
                                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="tab4">
                          <h3 className="mb-3">Ongoing conversations</h3>
                          <div className="row">
                            <div className="col-sm-6">
                              <div className="con_sec">
                                <h4>I have a payment or refund related query</h4>
                                <span className="date">Fri, May 3, 4:09 PM</span> <a href="" className="green_bt">RESUME CONVERSATION</a> </div>
                            </div>
                            <div className="col-sm-6">
                              <div className="con_sec">
                                <h4>I have a query related to placing an order</h4>
                                <span className="date">Fri, May 3, 4:09 PM</span> <a href="" className="green_bt">RESUME CONVERSATION</a> </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-sm-6">
                              <div className="con_sec">
                                <h4>I am unable to log in on BR Food</h4>
                                <span className="date">Fri, May 3, 4:09 PM</span> <a href="" className="green_bt">RESUME CONVERSATION</a> </div>
                            </div>
                            <div className="col-sm-6">
                              <div className="con_sec">
                                <h4>I have a coupon related query</h4>
                                <span className="date">Fri, May 3, 4:09 PM</span> <a href="" className="green_bt">RESUME CONVERSATION</a> </div>
                            </div>
                          </div>
                          <h3 className="mb-3">Ongoing conversations</h3>
                          <div className="row">
                            <div className="col-sm-6">
                              <div className="con_sec">
                                <h4>I am unable to log in on BR Food</h4>
                                <span className="date">Fri, May 3, 4:09 PM</span>
                                <div className="_3pVR7">Closed on Fri, May 3, 5:50 PM</div>
                              </div>
                            </div>
                            <div className="col-sm-6">
                              <div className="con_sec">
                                <h4>I have a coupon related query</h4>
                                <span className="date">Fri, May 3, 4:09 PM</span>
                                <div className="_3pVR7">Closed on Fri, May 3, 5:50 PM</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <Footer />
        </body>
      </Fragment>
    );
  }

  handleResponse = (nextProps) => {

    var respObj = null;
    if (nextProps.hasOwnProperty(Constants.KEY_SHOW_PROGRESS)) {
      respObj = { [Constants.KEY_SHOW_PROGRESS]: nextProps[Constants.KEY_SHOW_PROGRESS] };
      this.setState(respObj)
    }
    else if (nextProps.hasOwnProperty(Constants.KEY_RESPONSE)) {

      if (nextProps[Constants.KEY_TYPE] === types.API_SEND_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)

        this.setState({ disabledButton: false, [Constants.KEY_OTP]: nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA][Constants.KEY_OTP] });

        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: true, };
        this.setState(respObj);

      } else if (nextProps[Constants.KEY_TYPE] === types.API_VERIFY_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)
        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: false };

        this.setState(respObj);

        if (nextProps[Constants.KEY_RESPONSE].hasOwnProperty(Constants.KEY_DATA)) {

          CustomStorage.setSessionDataAsObject(Constants.KEY_USER_DATA, nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA]);

          window.location = Constants.SCREEN_HOME;
        }


      }
    }
  }

}


function mapStateToProps({ response }) {
  return response;
}


export default connect(mapStateToProps, { reqSendOtp, reqVerifyOtp })(Help);
