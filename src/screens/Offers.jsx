import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as Constants from '../utils/Constants';
import * as CustomStorage from '../utils/CustomStorage';
import * as Utility from '../utils/Utility';
import BaseComponent from '../common/BaseComponent';
import CustomPBar from '../common/CustomPBar';
import * as StringKeys from '../res/StringKeys';
import { connect } from 'react-redux';
import * as types from '../actions/types';
import * as Colors from '../res/Colors'
import { reqSendOtp, reqVerifyOtp } from '../actions'
import $ from 'jquery'
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Footer, NavBar } from '../common';
import * as ResConst from '../res/ResourcesConstants'

class Offers extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      [Constants.KEY_SHOW_PROGRESS]: false,
      [Constants.KEY_PHONE]: '',
      visibleOtp: false,
      [Constants.KEY_OTP]: '',
      disabledButton: true
    }
  }

  render() {
    const { className } = this.props;

    return (
      <Fragment>
        {/* <NavBar context={this} /> */}

        <section className="restaurant-part offer-part">

          <div className="container">
            <div id="horizontalTab">
              <ul className="resp-tabs-list">
                <li>Restaurant offers</li>
                <li>Payment offers/Coupons</li>
              </ul>
              <div className="resp-tabs-container">
                <div className="tab1">
                  <div className="offer-head mt-4">
                    <h2>Top Offers</h2>
                    <p>Top offers hand-picked for you</p>
                  </div>

                  <div className="row">
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.2</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="more-pro">
                        <a href="#">
                          +25 More
                            </a>
                      </div>
                    </div>

                  </div>


                  <div className="bdrtop mb-5"></div>

                  <div className="offer-head">
                    <h2>Great Brands, Great Deals</h2>
                    <p>Deals & discounts from all-time favorite brands</p>
                  </div>

                  <div className="row">
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul> 
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.2</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="more-pro">
                        <a href="#">
                          +25 More
                            </a>
                      </div>
                    </div>

                  </div>


                  <div className="bdrtop mb-5"></div>

                  <div className="offer-head">
                    <h2>Free Delivery</h2>
                    <p>Free delivery on orders above 99/-</p>
                  </div>

                  <div className="row">
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.2</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="more-pro">
                        <a href="#">
                          +25 More
                            </a>
                      </div>
                    </div>

                  </div>


                  <div className="bdrtop mb-5"></div>


                  <div className="offer-head">
                    <h2>Buy Some, Get Some</h2>
                    <p>Restaurants offering freebies</p>
                  </div>

                  <div className="row">
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag1">Promoted</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating"><i className="fa fa-star"></i> 4.1</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                      <div className="restaurant-col">
                        <img src={ResConst.restra_img} />
                        <span className="offer-tag2">Exclusive</span>
                        <div className="restra-inner">
                          <h3>Dana Pani</h3>
                          <p>Indian, Chines</p>
                          <ul>
                            <li>
                              <span className="rating red-color"><i className="fa fa-star"></i> 3.2</span>
                            </li>
                            <li>
                              28 MINS
                                    </li>
                            <li>
                              <i className="fa fa-rupee"></i> 200 For Two
                                    </li>
                          </ul>
                          <p className="offer-text"><i><img src={ResConst.discount} /></i> 40% off | Use coupon <strong>BRFOOD</strong></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="tab2">
                  <div className="offer-head mt-4">
                    <h3 className="mb-4">Available Coupons</h3>

                    <div className="row">

                      <div className="col-md-4 col-sm-6">
                        <div className="coupon-box">
                          <div className="coupon-tag"><i><img src={ResConst.locat_icon} /></i> BRFOOD</div>
                          <h5>Get 25% cashback using BRFOOD</h5>
                          <p>Use code BRFOOD & get 25% cashback up to Rs. 200 on your order above Rs. 500. Offer valid only on Fridays.</p>
                          <a href="#">KNOW MORE</a>
                          <a href="#" className="copy-btn">COPY CODE</a>
                        </div>
                      </div>
                      <div className="col-md-4 col-sm-6">
                        <div className="coupon-box">
                          <div className="coupon-tag"><i><img src={ResConst.pe_icon} /></i> PPEPOWERPLAY</div>
                          <h5>Get assured scratch card between Rs.10 to Rs.100 using Phone Pe</h5>
                          <p>Use code PPEPOWERPLAY & get 25% cashback up to Rs. 200 on your order above Rs. 500. Offer valid only on Fridays.</p>
                          <a href="#">KNOW MORE</a>
                          <a href="#" className="copy-btn">COPY CODE</a>
                        </div>
                      </div>
                      <div className="col-md-4 col-sm-6">
                        <div className="coupon-box">
                          <div className="coupon-tag"><i><img src={ResConst.paytm_icon} /></i> PMNEWAIR</div>
                          <h5>Get assured scratch card between Rs.10 to Rs.100 using Phone Pe</h5>
                          <p>Use code PPEPOWERPLAY & get assured scratch card between Rs.10 to Rs.100 on your order above Rs.99.</p>
                          <a href="#">KNOW MORE</a>
                          <a href="#" className="copy-btn">COPY CODE</a>
                        </div>
                      </div>

                    </div>

                    <h3 className="mb-4">Bank Offers</h3>

                    <div className="row">

                      <div className="col-md-4 col-sm-6">
                        <div className="bank-fooer-box">
                          <i><img src={ResConst.amazon_icon} /></i>
                          <h5>Get flat Rs.30 cashback using Amazon Pay</h5>
                          <p>Get flat Rs.30 cashback on orders above Rs.99 for 10 orders. No code required.</p>
                          <a href="#">KNOW MORE</a>
                        </div>
                      </div>
                      <div className="col-md-4 col-sm-6">
                        <div className="bank-fooer-box">
                          <i><img src={ResConst.hdfc_icon} /></i>
                          <h5>Get 10X reward points on HDFC Bank Diners Club Credit Cards</h5>
                          <p>Get 10X reward points on your HDFC Bank Diners Club Credit Cards on orders above Rs.150</p>
                          <a href="#">KNOW MORE</a>
                        </div>
                      </div>
                      <div className="col-md-4 col-sm-6">
                        <div className="bank-fooer-box">
                          <i><img src={ResConst.moviewik_icon} /></i>
                          <h5>Get 25% Supercash using Mobikwik</h5>
                          <p>Get flat Rs.30 cashback on orders above Rs.99 for 10 orders. No code required.</p>
                          <a href="#">KNOW MORE</a>
                        </div>
                      </div>
                      <div className="col-md-4 col-sm-6">
                        <div className="bank-fooer-box">
                          <i><img src={ResConst.amazon_icon} /></i>
                          <h5>Get flat Rs.30 cashback using Amazon Pay</h5>
                          <p>Get flat Rs.30 cashback on orders above Rs.99 for 10 orders. No code required.</p>
                          <a href="#">KNOW MORE</a>
                        </div>
                      </div>

                    </div>



                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>

        <Footer />
      </Fragment>
    );
  }

  handleResponse = (nextProps) => {

    var respObj = null;
    if (nextProps.hasOwnProperty(Constants.KEY_SHOW_PROGRESS)) {
      respObj = { [Constants.KEY_SHOW_PROGRESS]: nextProps[Constants.KEY_SHOW_PROGRESS] };
      this.setState(respObj)
    }
    else if (nextProps.hasOwnProperty(Constants.KEY_RESPONSE)) {

      if (nextProps[Constants.KEY_TYPE] === types.API_SEND_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)

        this.setState({ disabledButton: false, [Constants.KEY_OTP]: nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA][Constants.KEY_OTP] });

        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: true, };
        this.setState(respObj);

      } else if (nextProps[Constants.KEY_TYPE] === types.API_VERIFY_OTP) {
        console.log('handleResponse called in nextProps Add Category Screen  Kishan : ', nextProps)
        respObj = { [Constants.KEY_SHOW_PROGRESS]: false, visibleOtp: false };

        this.setState(respObj);

        if (nextProps[Constants.KEY_RESPONSE].hasOwnProperty(Constants.KEY_DATA)) {

          CustomStorage.setSessionDataAsObject(Constants.KEY_USER_DATA, nextProps[Constants.KEY_RESPONSE][Constants.KEY_DATA]);

          window.location = Constants.SCREEN_HOME;
        }


      }
    }
  }

}


function mapStateToProps({ response }) {
  return response;
}


export default connect(mapStateToProps, { reqSendOtp, reqVerifyOtp })(Offers);
