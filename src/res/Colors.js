export const white = '#ffffff'

export const appBg = '#EFEFEF';
export const red_color = '#F31A30';
export const red_light_color = '#FFE2E2';
export const transparent = '#00000000';
export const background_white_light = '#F8F8FA';
export const desabled_btn_clr = '#ff8282';




export const colorPrimary = '#F62C3A';
export const colorPrimaryDark = '#D41829';
export const colorAccent = '#F62C3A';
export const txt_color = '#000000';
export const KEY_PRIMARY = 'primary';
export const KEY_SECONDARY = 'secondary';
export const red_btn_clr = '#e43935'
export const black = '#000000'


