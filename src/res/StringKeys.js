export const Login = 'Login';
export const Signup='Signup';
export const Sign_Up='Sign_Up';
export const Save = 'Save';
export const Restaurant_Name = 'Restaurant_Name';
export const Mobile_Number = 'Mobile_Number';
export const Add_Category = 'Add_Category';
export const Category_Name = 'Category_Name';
export const Remember_Me = 'Remember_Me';
export const Dashboard = 'Dashboard';
export const Restaurants = 'Restaurants';
export const Add_Restaurant = 'Add_Restaurant';
export const Restaurant_List = 'Restaurant_List';
export const Categories = 'Categories';
export const Add_Categories = 'Add_Categories';
export const Add_Drivers = 'Add_Drivers';
export const Drivers = 'Drivers';
export const Drivers_List = 'Drivers_List';
export const Integrations = 'Integrations';
export const Categories_List = 'Categories_List';
export const Logout = 'Logout';
export const Reports = 'Reports';
export const Requested_Driver = 'Requested_Driver';
export const Requested_Restaurent = 'Requested_Restaurent';
export const Enter_Your_Delivery_Location='Enter_Your_Delivery_Location';
export const Hungry='Hungry';
export const Order_Food_From_Favourite ='Order_Food_From_Favourite';
export const Login_To_Your_Account='Login_To_Your_Account';
export const Or='Or';
export const Create_An_Account='Create_An_Account';
export const Phone_Number='Phone_Number';
export const Verify_Otp='Verify_Otp';
export const Continue='Continue';
export const By_Creating_An_Account='By_Creating_An_Account';
export const Find_Food='Find_Food';
export const FIND_FOOD='FIND_FOOD';
export const Popular_Cities='Popular_Cities';
export const Ahmedabad="Ahmedabad";
export const Bangalore="Bangalore";
export const Chennai="Chennai";
export const Delhi="Delhi";
export const Gurgaon="Gurgaon";
export const Hyderabad="Hyderabad";
export const Kolkata="Kolkata";
export const Mumbai="Mumbai";
export const Pune="Pune";
export const More="More";
export const No_Minimum_Order="No_Minimum_Order";
export const Order_In_For="Order_In_For";
export const Live_Order_Tracking="Live_Order_Tracking";
export const Know_Where_Your="Know_Where_Your";
export const Lightning_Fast_Delivery="Lightning_Fast_Delivery";
export const Experience_Br_Food="Experience_Br_Food";
export const Restaurants_In_Your_Pocket="Restaurants_In_Your_Pocket";
export const Order_From_Your="Order_From_Your";
export const CONTACT="CONTACT";
export const COMPANY="COMPANY";
export const About_Us="About_Us";
export const Team="Team";
export const Careers="Careers";
export const BR_Food_Blog="BR_Food_Blog";
export const Bug_Bounty="Bug_Bounty";
export const BR_Food_Pop="BR_Food_Pop";
export const BR_Food="BR_Food";
export const Help_Support="Bug_Bounty";
export const Partner_with_us="BR_Food_Pop";
export const Ride_with_us="BR_Food";
export const LEGAL="LEGAL";
export const Terms_Conditions='Terms_Conditions';
export const Refund_Cancellation="Refund_Cancellation";
export const Privacy_Policy="Privacy_Policy";
export const Cookie_Policy="Cookie_Policy";
export const Offer_Terms="Offer_Terms";
export const WE_DELIVER_TO="WE_DELIVER_TO";
export const Copyright_2019="Copyright_2019";
export const Get_current_location="Get_current_location";
export const Using_GPS="Using_GPS";
export const SAVED_ADDRESSES="SAVED_ADDRESSES";
export const Work ="Work";
export const Other="Other";
export const Filters="Filters";
export const CLEAR="CLEAR";
export const SHOW_RESTAURANTS="SHOW_RESTAURANTS";
export const Offer="Offer";
export const Introducing_BR="Introducing_BR";
export const Single_Serve_Meals="Single_Serve_Meals";
export const Order_Now="Order_Now";
export const Introducing_BR_Food_SUPER="Introducing_BR_Food_SUPER";
export const Relevance_Cost="Relevance_Cost";
export const for_Two="for_Two";
export const Delivery_Time="Delivery_Time";
export const Rating="Rating";
export const Filter="Filter";
export const Get_Super_Now="Get_Super_Now";
export const Promoted="Promoted";
export const MINS="MINS";
export const You_Are_Offline="You_Are_Offline";
export const For_Two="For_Two";











