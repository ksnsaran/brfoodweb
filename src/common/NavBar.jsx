import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Footer } from '../common';
import $ from 'jquery'
import * as ResConst from '../res/ResourcesConstants'
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
class NavBar extends React.Component {

    render() {

        const { context } = this.props;
        return (
            <Fragment>

                <nav className="navbar navbar-expand-md navbar-dark fixed-top" id="banner" >
                    <div className="container">
                        {/* <!-- Brand --> */}
                        <a className="navbar-brand logo2" href="index.html"><img src={ResConst.locat_icon} /></a>

                        <div className="location-add">
                            <a href="#">Vidyadhar Nagar</a>
                            <span className="show-address">E-12, Ambabari, Vidyadhar Nagar, Jaipur, Rajasthan 302039, India</span>
                            <a className="search-slide" onClick={() => context.toggle('location')}><i className="fa fa-chevron-down"></i></a>
                        </div>

                        {/* <!-- Toggler/collapsibe Button --> */}
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        {/* <!-- Navbar links --> */}
                        <div className="collapse navbar-collapse" id="collapsibleNavbar">
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item" >
                                    <a className="nav-link" onClick={() => window.location = '/search'} style={{ color: 'black' }}><i className="fa fa-search"></i> Search</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => window.location = '/offers'} style={{ color: 'black' }}><i className="fa fa-tag"></i> Offers <span>New</span></a>
                                </li>
                                {/* <!-- Dropdown --> */}
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown" style={{ color: 'black' }}>
                                        <i className="fa fa-shopping-cart"></i> Cart
                                    </a>
                                    <div className="dropdown-menu cart-box" style={{ width: '300px' }}>
                                        <div className="row">
                                            <div className="col-4">
                                                <div className="cart-dish"><img src={ResConst.offer_img} /></div>
                                            </div>
                                            <div className="col-8">
                                                <div className="cart-head">
                                                    <h5>DMB(Doodh Misthan Bhandar)</h5>
                                                    <p className="small">Lal Kothi</p>
                                                </div>
                                            </div>
                                            <div className="bdrtop"></div>
                                            <div className="col-9 text-left">
                                                <i></i> Akhrot Barfi x 1
                                            </div>
                                            <div className="col-3 text-right">
                                                <i className="fa fa-rupee"></i> 95
                                             </div>
                                            <div className="bdrtop"></div>
                                            <div className="col-9 text-left">
                                                <strong>Sub Total</strong>
                                                <small>Extra charges may apply</small>
                                            </div>
                                            <div className="col-3 text-right">
                                                <i className="fa fa-rupee"></i> 95
                                            </div>

                                            <button className="cart-check-btn">Checkout</button>

                                        </div>
                                    </div>
                                </li>


                                <li className="nav-item">
                                    <a className="nav-link" onClick={() => window.location = '/help'} style={{ color: 'black' }}><i className="fa fa-question-circle"></i> Help</a>
                                </li>
                                <li className="nav-item dropdown" >
                                    <a className="nav-link" href="#" onClick={() => context.toggle('OTP')} style={{ color: 'black' }} s> <i className="fa fa-user"></i> Sign In </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </Fragment>
        );
    }
}



const connectedNavBar = connect()(NavBar);
export { connectedNavBar as NavBar }; 