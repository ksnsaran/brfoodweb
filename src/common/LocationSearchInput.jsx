import React from 'react';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import { withStyles } from '@material-ui/core/styles';
import * as Constants from '../utils/Constants'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const styles = {
  root: {
    border: 0,
    borderRadius: 3,
    color: 'black',
    width:500
  },
  input:{
    width:400,
    height:60
  }
};

class LocationSearchInput extends React.Component {
    constructor(props) {
      super(props);
      this.state = { address: '' };
    }
   
    handleChange = address => {
      this.setState({ address });
    };
   
    handleSelect = address => {
      geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(latLng => {console.log('Success', latLng)
        if (sessionStorage.getItem(Constants.KEY_USER_DATA) === undefined || sessionStorage.getItem(Constants.KEY_USER_DATA) === null) {
          alert('Please login to continue.');
        }

        window.location = Constants.SCREEN_HOME;
      })
        .catch(error => console.error('Error', error));
    };
   
    render() {
      const { classes } = this.props;

      return (
        <PlacesAutocomplete
          value={this.state.address}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
        >
          {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
            <div>
            <div className="search-location-part">
              <input 
                {...getInputProps({
                  placeholder: 'Enter your delivery location',
                  className: 'location-input',
                })}
              />
              <input type="text" name="" id="" placeholder="Locat Me" className="location-input2" />
              <button className="btn find-food-btn">FIND FOOD</button>

             </div>
           <div className="autocomplete-dropdown-container">
            
                {loading && <div>Loading...</div>}
                {suggestions.map(suggestion => {
                  const className = suggestion.active
                    ? 'suggestion-item--active'
                    : 'suggestion-item';
                  // inline style for demonstration purpose
                  const style = suggestion.active
                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                  return (
                    <div 
                      {...getSuggestionItemProps(suggestion, {
                        className,
                        style,
                      })}
                    >
                      <span >{suggestion.description}</span>
                    </div>
                  );
                })}
              </div>
            </div>
          )}
        </PlacesAutocomplete>
      );
    }
  }


  export default withStyles(styles)(LocationSearchInput);