import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import $ from 'jquery'
import * as ResConst from '../res/ResourcesConstants'

class Footer extends React.Component {



    toggleModal(divId) {
        if (divId == 'login') {
            $("#" + divId).show();
            $("#" + 'signup').hide();
        }
        if (divId == 'signup') {
            $("#" + divId).show();
            $("#" + 'login').hide();
        }
    }

    closeModal(divId) {
        $("#" + divId).hide()
    }



    render() {

        return (
            <Fragment>
                {/* footer section */}
                <footer>
                    <div className="container">
                        <div className="row footer-col1">
                            <div className="col-sm-3">
                                <h3>COMPANY</h3>
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h3>CONTACT</h3>
                                <ul>
                                    <li><a href="#">Help & Support</a></li>
                                    <li><a href="#">Partner with us</a></li>
                                    <li><a href="#">Ride with us</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <h3>LEGAL</h3>
                                <ul>
                                    <li><a href="#">Terms & Conditions</a></li>
                                    <li><a href="#">Refund & Cancellation</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Cookie Policy</a></li>
                                    <li><a href="#">Offer Terms</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <div className="footer-app-icon">
                                    <div className="mobile-app">
                                        <a href="#"><img src={ResConst.play_icon} /></a>
                                        <a href="#"><img src={ResConst.iOS_icon} /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row footer-col1">
                            <div className="col-sm-12 mb-2">
                                <h3>WE DELIVER TO</h3>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                            <div className="col-sm-3">
                                <ul>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">BR Food Blog</a></li>
                                    <li><a href="#">Bug Bounty</a></li>
                                    <li><a href="#">BR Food Pop</a></li>
                                    <li><a href="#">BR Food</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="copyright">
                            <div className="row">
                                <div className="col-sm-12 text-center">
                                    <p>&copy; Copyright 2019. BR FOOOD.</p>
                                    <ul>
                                        <li><a href="#"><i className="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                                        <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                {/* footer section */}
            </Fragment>
        );
    }
}



const connectedFooter = connect()(Footer);
export { connectedFooter as Footer }; 