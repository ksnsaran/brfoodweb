import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl'
import { createStyles, makeStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
class BaseComponent extends Component {


  // handleBackButtonClick = () => {
  //   this.props.goBack();
  //   return true;
  // }

  showLinearPBar(value) {
    return <div>
      {value &&
        <LinearProgress />
      }
    </div>
  }

  strings(name) {
    return <FormattedMessage
      id={name}
    />
  };

  goBack = () => {

    window.history.back();
  }
}
export default BaseComponent;
