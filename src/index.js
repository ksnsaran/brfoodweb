import React from "react";
import ReactDOM from "react-dom";
import App from '../src/App.jsx'
import { IntlProvider, addLocaleData } from "react-intl";
import esLocaleData from "react-intl/locale-data/es";
import svLocaleData from "react-intl/locale-data/sv";
import enLocaleData from "react-intl/locale-data/en";
import translations from '../src/i18n/locales'
import '../css/bootstrap.min.css'
import '../css/font-awesome.min.css'
import '../css/style.css'
import '../css/responsive.css'
import '../css/slick.css'
import '../css/easy-responsive-tabs.css'



addLocaleData([...esLocaleData, ...svLocaleData, ...enLocaleData]);


const locale = window.location.search.replace("?locale=", "") || "en"
const messages = translations[locale];

ReactDOM.render(
  <IntlProvider locale={locale} messages={messages}>
    <App />
  </IntlProvider>,
  document.getElementById("root")
);

