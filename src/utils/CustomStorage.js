import * as  Constants from './Constants';

export const setSessionDataAsObject = (keyAsString, value) => {
    sessionStorage.setItem(keyAsString, JSON.stringify(value));
}

export const getSessionDataAsObject = (keyAsString) => {
    var objData = JSON.parse(sessionStorage.getItem(keyAsString));
    console.log(keyAsString + ' value : ', objData);
    return objData;
}

export const clearLocalData =  () => {

    try {
         localStorage.clear();

    } catch (error) {
        alert('error ' + error);
        console.log('logout 2 error ::::: ', error);
        // Error retrieving data
    }

}

export const clearSessionData =  () => {

    try {
        sessionStorage.clear();
        
    } catch (error) {
        alert('error ' + error);
        console.log('logout 2 error ::::: ', error);
        // Error retrieving data
    }

}