import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import BaseComponent from '../common/BaseComponent';
import { Route, Redirect } from "react-router-dom";
import * as  CustomStorage from '../utils/CustomStorage';
import * as  Constants from '../utils/Constants';
import * as Colors from '../res/Colors';
import Avatar from '@material-ui/core/Avatar';
import * as ResConst from '../res/ResourcesConstants'
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/KeyboardArrowDown';
import SearchIcon from '@material-ui/icons/Search';
import LocalOffer from '@material-ui/icons/LocalOffer';
import HelpIcon from '@material-ui/icons/Help';
import Person from '@material-ui/icons/Person';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import LocationView from './LocationView'
import CommonAppBar from './../common/CommonAppBar'
//Drawer

import { colors, Grid } from '@material-ui/core';


class Home extends BaseComponent {
  state = {
    open: true,
  };


  render() {
    const { classes } = this.props;
    if (sessionStorage.getItem(Constants.KEY_USER_DATA) === undefined || sessionStorage.getItem(Constants.KEY_USER_DATA) === null) {
      window.location = Constants.SCREEN_LOGIN;
    }
    return (
      <div className={classes.root}>
        <CommonAppBar
          renderRestInfo={<main className={classes.content}>
            {this.props.children}
          </main>}
        />

        <nav class="navbar navbar-expand-md navbar-dark fixed-top" id="banner">
        <div class="container">
      
        <a class="navbar-brand logo2" href="#"><img src="images/logo-icon.png"/></a>
        
        <div class="location-add">
            <a href="#">Vidyadhar Nagar</a> <span class="show-address">E-12, Ambabari, Vidyadhar Nagar, Jaipur, Rajasthan 302039, India</span> <span class="search-slide"><i class="fa fa-chevron-down"></i></span>
        </div>
        
      
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span class="navbar-toggler-icon"></span>
        </button>
      
      
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fa fa-search"></i> Search</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fa fa-tag"></i> Offers <span>New</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="fa fa-question-circle"></i> Help</a>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              <i class="fa fa-user"></i> Sign In
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Link 1</a>
              <a class="dropdown-item" href="#">Link 2</a>
              <a class="dropdown-item" href="#">Link 3</a>
            </div>
           </li>
      
           <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              <i class="fa fa-shopping-cart"></i> Cart
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#">Link 1</a>
              <a class="dropdown-item" href="#">Link 2</a>
              <a class="dropdown-item" href="#">Link 3</a>
            </div>
           </li>
          </ul>
        </div>
        </div>
      </nav>
      


      </div>
    );
  }

  onItemClickHandler = (event) => {
    event.preventDefault();
    CustomStorage.clearSessionData();
    window.location.reload();
    // history.go(-1);
    // history.goBack();
  }
}



const GoToRightScreen = ({ component: Component, ...rest }) => {
  console.log('gotorightscreen', sessionStorage.getItem(Constants.KEY_USER_DATA))
  var properties = { ...rest }
  const classes = properties.classes
  return (
    (sessionStorage.getItem(Constants.KEY_USER_DATA) !== undefined && sessionStorage.getItem(Constants.KEY_USER_DATA) !== null) ?
      (<Route {...rest} render={() => (

        <Home classes={classes}>
          <Component classes={classes} />
        </Home>

      )} />) : <Redirect from="/" to={Constants.SCREEN_LOGIN} />
  )
};

const styles = theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
    width: '100vw',
    height: '100vh',
    backgroundColor: Colors.white
  },
  logoAvatar: {
    padding: 1,
    marginLeft: 60
    //color: '#fff',

  },

  appBar: {
    backgroundColor: 'white',
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  title: {

    fontSize: 18,
    color: Colors.red_color

  },
  title2: {
    marginLeft: 5,
    // flexGrow: 1,
    fontSize: 15,
    color: Colors.black

  },

  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },

});

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(GoToRightScreen);