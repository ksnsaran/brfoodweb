import React from 'react';
import { makeStyles,withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import * as Colors from '../res/Colors'
import GooglePlacesAutocomplete from 'react-google-places-autocomplete';


function LocationView() {
  // const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  const sideList = side => (
    <div
      // className={classes.list}
      style={{width:500,}}
      role="presentation"
     // onClick={toggleDrawer(side, false)}
      //onKeyDown={toggleDrawer(side, false)}
    >
    
      <div style={{borderWidth:5,borderColor:'black'}}>
      <GooglePlacesAutocomplete
        placeholder='Search for area ,street name'
        inputStyle={{ borderWidth: 10, borderColor: 'black',  }}
        suggestionsStyles={{ borderWidth: 10, borderColor: 'black' }}
        onSelect={console.log}
      />
      </div>
    </div>
  );


  return (
    <div style={{justifyContent:'center',alignItems:'center',display:'flex'}}>
      {/* <Button onClick={toggleDrawer('left', true)}>Open Left</Button> */}
      <KeyboardArrowDown nativeColor={Colors.black} onClick={toggleDrawer('left', true)} />
      <Drawer open={state.left} onClose={toggleDrawer('left', false)}>
        {sideList('left')}
      </Drawer>
     
    </div>
  );
}

export default LocationView;