
//Screens
export const SCREEN_LOGIN = "/login_signup";

export const SCREEN_HOME = "/homepage";
export const SCREEN_SEARCH = "/search";
export const SCREEN_OFFERS = "/offers";
export const SCREEN_HELP = "/help";






export const KEY_ROLE = "role"
export const KEY_USER ="user";
export const KEY_DATA = "data";
export const KEY_SHOW_PROGRESS = "show_progress";
export const KEY_RESPONSE = "response";
export const KEY_TYPE = "type";
export const KEY_USER_DATA = "user_data";
export const KEY_MESSAGE = "message";
export const KEY_ERROR = "error";
export const KEY_TITLE = "title";
export const KEY_STATUS = "status";
export const KEY_DESCRIPTION = "description";

export const KEY_NAME = "name";
export const KEY_EMAIL = "email";
export const KEY_ADDRESS = "address";
export const KEY_ZIP = "zip";
export const KEY_OUTLETS = "outlets";
export const KEY_COSTOFTWO = "costoftwo";
export const KEY_DELIVERYSUPPORT = "deliverysupport";
export const KEY_LAT = "lat";
export const KEY_LNG = "lng";
export const KEY_STATE = "state";
export const KEY_CITY = "city";
export const KEY_LANDMARK = "landmark";
export const KEY_CATEGORIES = "categories";
export const KEY_PERSON_COUNT = "person_count";
export const KEY_IMAGE = "image";
export const KEY_IMAGE_UPLOAD = "image_upload";

export const KEY_IMAGES_ARRAY = 'images[';
export const KEY_ARRAY_CLOSE = ']';
export const KEY_DOCUMENTS_ARRAY = 'documents[';
export const KEY_HEADERS = "headers";
export const KEY_BODY = "body";
export const KEY_TOKEN = "token";
export const KEY_COUNTRY_CODE_VALUE = "91";
export const KEY_COUNTRY_CODE = "country_code";
export const KEY_PHONE = "phone";
export const KEY_DEVICE_ID= "device_id";
export const KEY_DEVICE_TYPE= "device_type";
export const KEY_DEVICE_TOKEN= "device_token";
export const KEY_OTP = "otp";
export const KEY_OFFSET="offset";
export const KEY_RESPOBJ="respObj";
export const KEY_VISIBLE_OTP="visibleOtp";
export const KEY_DISABLE_BUTTON="disabledButton";








export const MAX_LENGTH_OTP = 4;
export const MAX_LENGTH_PHONE = 10;









